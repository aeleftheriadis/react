module.exports = {
  // All the components you'd like to render server-side
  VariableValuesDisplay: require('./VariableValuesDisplay').default,
  PosRecovery: require('./PosRecovery').default
};
