import React, { Component,PropTypes } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Table } from 'react-bootstrap';
import AlertDismissable from './AlertDismissable.jsx';

/**
 * Class Results Table
 * @extends React.Component
 */
export default class ResultsTable extends React.Component {
  /**
   * constructor
   * @param  {Object} props
   */
  constructor(props){
    super(props);
  }
  /**
   * onSelectAll input event handler
   * @param  {Object} event
   * @param  {Object} allCheckedValues
   * @param  {Object} filePath
   */
  onSelectAll(event,allCheckedValues,filePath){
      let documentOids = allCheckedValues.map(element => element.DocumentHeaderOid);

      let allSelectedInputs = ReactDOM.findDOMNode(this).getElementsByTagName('input');

      let inputSelectAll = $(allSelectedInputs).filter(function(){return $(this).hasClass('isSelectAll') });
      var isChecked = $(inputSelectAll).is(':checked');

      $(allSelectedInputs).each(function(){
        if(!$(this).hasClass('isSelectAll') ){
          $(this).prop('checked', isChecked);
        }
      });
      if(event.target.checked){
        this.props.onSelectAll(documentOids,filePath);
      }
      else{
        this.props.onSelectAll([],filePath);
      }
  }
  /**
   * Render ResultsTable Component
   * @return {Object} React View
   */
  render(){
    return(
      <ReactCSSTransitionGroup transitionName="opacityEaseInOutExpo" transitionAppear={true} transitionAppearTimeout={200} transitionEnterTimeout={200} transitionLeaveTimeout={200}>
        <Table striped bordered condensed hover responsive>
          <thead>
            <tr>
              <th><input className="Checkbox-Checkbox isSelectAll" type="checkbox" onChange={event => this.onSelectAll(event,this.props.results,this.props.filePath) } /></th>
              <th>Status</th>
              <th>GrossTotal</th>
              <th>ExistingGrossTotal</th>
              <th>Difference</th>
              <th>DocumentType</th>
              <th>DocumentSeries</th>
              <th>Customer</th>
              <th>DocumentNumber</th>
              <th>FiscalDate</th>
            </tr>
          </thead>
          <tbody>

            {(this.props.results).map(results => {
              const key = results.DocumentHeaderOid;
              return(
                <tr key={key}>
                  <td>
                    <input className="Checkbox-Checkbox" type="checkbox" onChange={event => this.props.onChange(event,key,this.props.filePath)}/>
                  </td>
                  <td>{results.Status}</td>
                  <td>{results.GrossTotal}</td>
                  <td>{results.ExistingGrossTotal}</td>
                  <td>{results.Difference}</td>
                  <td>{results.DocumentType}</td>
                  <td>{results.DocumentSeries}</td>
                  <td>{results.Customer}</td>
                  <td>{results.DocumentNumber}</td>
                  <td>{results.FiscalDate}</td>
                </tr>
              );
            })}

          </tbody>
        </Table>

      </ReactCSSTransitionGroup>
    )
  }
}
